// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Draw planet with transparency
// Chapter 5 spacewar.cpp v1.0
// This class is the core of the game

#include "spaceWar.h"
#include <ctime>
#include <string>
#include <sstream>

//=============================================================================
// Constructor
//=============================================================================
Spacewar::Spacewar()
{
	textFont = new TextDX();
	gameOverFont = new TextDX();
	playerSwingPlayed = false;
	comSwingPlayed = false;
	hitPlayed = false;
	collided = false;
	playerScore = 0;
	comScore = 0;
	randSwing = 0;
	randHit = 0;
}

//=============================================================================
// Destructor
//=============================================================================
Spacewar::~Spacewar()
{
	audio->playCue(LS_OFF);
	Sleep(450);				// for playing the "turning off" effect and hearing it
	releaseAll();           // call onLostDevice() for every graphics item
	SAFE_DELETE(textFont);
}

//=============================================================================
// Initializes the game
// Throws GameError on error
//=============================================================================
void Spacewar::initialize(HWND hwnd)
{
	Game::initialize(hwnd); // throws GameError

	std::srand(time(0));

	if (!lightsaberTexture.initialize(graphics, LIGHTSABER_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Lightsaber texture initialization failed"));
	if (!lightsaber.initialize(graphics, 0,0,0, &lightsaberTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init lightsaber"));

	if (!lightsaberTexture2.initialize(graphics, LIGHTSABER_IMAGE2))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Lightsaber texture 2 initialization failed"));
	if (!lightsaber2.initialize(graphics, 0,0,0, &lightsaberTexture2))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init lightsaber2"));

	if (!deathStarTexture.initialize(graphics, DEATHSTAR_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Death Star texture initialization failed"));
	if (!deathStar.initialize(graphics, 0,0,0, &deathStarTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init Death Star"));

	if (!backgroundTexture.initialize(graphics, BACKGROUND_IMAGE))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Background texture initialization failed"));
	if (!background.initialize(graphics, 0,0,0, &backgroundTexture))
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error init background"));

	if(textFont->initialize(graphics, 32, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));
	if(gameOverFont->initialize(graphics, 100, true, false, "Arial") == false)
		throw(GameError(gameErrorNS::FATAL_ERROR, "Error initializing DirectX font"));

	lightsaber.setX(GAME_WIDTH - (lightsaber.getWidth()*LIGHTSABER_IMAGE_SCALE)*1.5);
	lightsaber.setY(GAME_HEIGHT/2 - (lightsaber.getHeight()*LIGHTSABER_IMAGE_SCALE)/2);
	lightsaber.setScale(LIGHTSABER_IMAGE_SCALE);

	lightsaber2.setX(0 + (lightsaber2.getWidth()*LIGHTSABER_IMAGE_SCALE));
	lightsaber2.setY(GAME_HEIGHT/2 - (lightsaber2.getHeight()*LIGHTSABER_IMAGE_SCALE)/2);
	lightsaber2.setScale(LIGHTSABER_IMAGE_SCALE);

	deathStar.setX(GAME_WIDTH/2 - (deathStar.getWidth()*DEATHSTAR_IMAGE_SCALE)/2);
	deathStar.setY(GAME_HEIGHT/2 - (deathStar.getHeight()*DEATHSTAR_IMAGE_SCALE)/2);
	deathStar.setScale(DEATHSTAR_IMAGE_SCALE);

	background.setX(GAME_WIDTH/2 - (background.getWidth()*BACKGROUND_IMAGE_SCALE)/2);
	background.setY(GAME_HEIGHT/2 - (background.getHeight()*BACKGROUND_IMAGE_SCALE)/2);
	background.setScale(BACKGROUND_IMAGE_SCALE);

	//Stuff for physics
	lightsaberPos.xPos = lightsaber.getX();
	lightsaberPos.yPos = lightsaber.getY();

	lightsaberPos2.xPos = lightsaber2.getX();
	lightsaberPos2.yPos = lightsaber2.getY();

	deathStarPos.xPos = deathStar.getX();
	deathStarPos.yPos = deathStar.getY();

	//Velocities
	lightsaberVel.xVel = 240;
	lightsaberVel.yVel = 240;

	lightsaberVel2.xVel = 240;
	lightsaberVel2.yVel = 240;

	deathStarVel.xVel = rand()%2;		//randomizes starting direction
	if(deathStarVel.xVel == 0) {
		deathStarVel.xVel = -300;
	}
	else {
		deathStarVel.xVel = 300;
	}
	deathStarVel.yVel = (rand()%720) - 360;
	if(deathStarVel.yVel >= -180 && deathStarVel.yVel < 0)	// in order to guarantee we have some vertical velocity
		deathStarVel.yVel = -180;
	else if(deathStarVel.yVel <= 180 && deathStarVel.yVel >= 0) 
		deathStarVel.yVel = 180;

	// starts the opening and looping cues
	audio->playCue(LS_ON); 
	audio->playCue(IDLE);
	audio->playCue(IMPERIAL);
	return;

}

//=============================================================================
// Update all game items
//=============================================================================
void Spacewar::update()
{
	////////////////
	// INPUT MODS
	////////////////

	D3DXVECTOR2 direction(0,0);

	randSwing = std::rand() % 4;

	if (input->isKeyDown(VK_RIGHT)) 
		direction.x = 1;
	if (input->isKeyDown(VK_LEFT)) 
		direction.x = -1;
	if (input->isKeyDown(VK_DOWN)) {
		direction.y = 1;
		if (!playerSwingPlayed) {
			switch(randSwing) {
			case 0: audio->playCue(SWING1); break;
			case 1: audio->playCue(SWING2); break;
			case 2: audio->playCue(SWING3); break;
			case 3: audio->playCue(SWING4); break;
			default: audio->playCue(SWING1); break;
			}
			playerSwingPlayed = true;
		}
	}
	if (input->isKeyDown(VK_UP)) {
		direction.y = -1;
		if (!playerSwingPlayed) {
			switch(randSwing) {
			case 0: audio->playCue(SWING1); break;
			case 1: audio->playCue(SWING2); break;
			case 2: audio->playCue(SWING3); break;
			case 3: audio->playCue(SWING4); break;
			default: audio->playCue(SWING1); break;
			}
			playerSwingPlayed = true;
		}
	}
	if (!input->isKeyDown(VK_UP) && !input->isKeyDown(VK_DOWN)) {
		playerSwingPlayed = false;
	}

	if(direction.x != 0 && direction.y != 0) 
		D3DXVec2Normalize(&direction, &direction);

	lightsaberPos.yPos = lightsaber.getY() + lightsaberVel.yVel * frameTime;
	lightsaber.setY(lightsaberPos.yPos);

	lightsaberPos2.yPos = lightsaber2.getY() + lightsaberVel2.yVel * frameTime * direction.y;
	lightsaber2.setY(lightsaberPos2.yPos);

	deathStarPos.xPos = deathStar.getX() + deathStarVel.xVel * frameTime;
	deathStar.setX(deathStarPos.xPos);
	deathStarPos.yPos = deathStar.getY() + deathStarVel.yVel * frameTime;
	deathStar.setY(deathStarPos.yPos);
}

//=============================================================================
// Artificial Intelligence
//=============================================================================
void Spacewar::ai()
{
	randSwing = std::rand() % 4;

	if ((deathStarPos.xPos + deathStar.getWidth() * DEATHSTAR_IMAGE_SCALE) >= GAME_WIDTH/4) {
		if (deathStarPos.yPos + deathStar.getHeight() >= lightsaberPos.yPos + lightsaber.getHeight()/2) {
			lightsaberVel.yVel = 240;
			if (!comSwingPlayed) {
				switch(randSwing) {
				case 0: audio->playCue(SWING1); break;
				case 1: audio->playCue(SWING2); break;
				case 2: audio->playCue(SWING3); break;
				case 3: audio->playCue(SWING4); break;
				default: audio->playCue(SWING1); break;
				}
				comSwingPlayed = true;
			}
		}
		if (deathStarPos.yPos + deathStar.getHeight() <= lightsaberPos.yPos + lightsaber.getHeight()/2) {
			lightsaberVel.yVel = -240;
			if (!comSwingPlayed) {
				switch(randSwing) {
				case 0: audio->playCue(SWING1); break;
				case 1: audio->playCue(SWING2); break;
				case 2: audio->playCue(SWING3); break;
				case 3: audio->playCue(SWING4); break;
				default: audio->playCue(SWING1); break;
				}
				comSwingPlayed = true;
			}
		}
	}

	if ((deathStarPos.xPos + deathStar.getWidth() * DEATHSTAR_IMAGE_SCALE) < GAME_WIDTH/4) {
		lightsaberVel.yVel = 0;
		comSwingPlayed = false;
	}
}

//=============================================================================
// Handle collisions
//=============================================================================
void Spacewar::collisions()
{
	/*********Player Controlled Lightsaber restricted to screen space*********/
	if (lightsaberPos2.yPos + lightsaber2.getHeight() * LIGHTSABER_IMAGE_SCALE >= GAME_HEIGHT) {
		lightsaberPos2.yPos = (GAME_HEIGHT - lightsaber2.getHeight() * LIGHTSABER_IMAGE_SCALE) - 2;
		lightsaber2.setY(lightsaberPos2.yPos);
	}
	if (lightsaberPos2.yPos <= 0) {
		lightsaberPos2.yPos = 2;
		lightsaber2.setY(lightsaberPos2.yPos);
	}

	/*********AI Controlled Lightsaber restricted to screen space*********/
	if (lightsaberPos.yPos + lightsaber.getHeight() * LIGHTSABER_IMAGE_SCALE >= GAME_HEIGHT) {
		lightsaberPos.yPos = (GAME_HEIGHT - lightsaber.getHeight() * LIGHTSABER_IMAGE_SCALE) - 2;
		lightsaber.setY(lightsaberPos.yPos);
	}
	if (lightsaberPos.yPos <= 0) {
		lightsaberPos.yPos = 2;
		lightsaber.setY(lightsaberPos.yPos);
	}

	/***Collision with AI controlled lightsaber***/
	randHit = std::rand() % 2;

	if(lightsaberPos.xPos <= deathStarPos.xPos + deathStar.getWidth() * DEATHSTAR_IMAGE_SCALE 
		&& deathStarPos.yPos >= lightsaberPos.yPos 
		&& deathStarPos.yPos + deathStar.getHeight() * DEATHSTAR_IMAGE_SCALE <= lightsaberPos.yPos + lightsaber.getHeight() * LIGHTSABER_IMAGE_SCALE
		&& !collided)
	{
		collided = true;
		deathStarVel.xVel *= -1.02;
		deathStarVel.yVel *= 1.05;
		if (!hitPlayed) {
			switch(randHit) {
			case 0: audio->playCue(HIT); break;
			case 1: audio->playCue(HIT2); break;
			default: break;
			}
			hitPlayed = true;
		}
	}
	/***Collision with Human controlled lightsaber***/
	if(lightsaberPos2.xPos + lightsaber2.getWidth() * LIGHTSABER_IMAGE_SCALE >= deathStarPos.xPos
		&& deathStarPos.yPos >= lightsaberPos2.yPos 
		&& deathStarPos.yPos + deathStar.getHeight() * DEATHSTAR_IMAGE_SCALE <= lightsaberPos2.yPos + lightsaber2.getHeight() * LIGHTSABER_IMAGE_SCALE
		&& !collided) 
	{
		collided = true;
		deathStarVel.xVel *= -1.02;
		deathStarVel.yVel *= 1.05;
		if (!hitPlayed) {
			switch(randHit) {
			case 0: audio->playCue(HIT); break;
			case 1: audio->playCue(HIT2); break;
			default: break;
			}
			hitPlayed = true;
		}
	}

	// when the ball is at the midpoint, reset collided and hitPlayed variables
	if(deathStarPos.xPos > GAME_WIDTH/2 - 20 && deathStarPos.xPos <= GAME_WIDTH/2 + 20) {
		collided = false;
		hitPlayed = false;
	}

	/*********Death Star Bouncing*********/
	if ((deathStarPos.xPos + deathStar.getWidth() * DEATHSTAR_IMAGE_SCALE) + 2 >= GAME_WIDTH) {
		deathStarVel.xVel *= -1;
		deathStarPos.xPos = GAME_WIDTH - deathStar.getWidth() * DEATHSTAR_IMAGE_SCALE - 2;
		deathStar.setX(deathStarPos.xPos);
		playerScore++;
		resetGame(true);
	}
	if (deathStarPos.xPos - 2 <= 0) {
		deathStarVel.xVel *= -1;
		deathStarPos.xPos = 2;
		deathStar.setX(deathStarPos.xPos);
		comScore++;
		resetGame(false);
	}
	if ((deathStarPos.yPos + deathStar.getHeight() * DEATHSTAR_IMAGE_SCALE) + 2 >= GAME_HEIGHT) {
		deathStarVel.yVel *= -1;
		deathStarPos.yPos = GAME_HEIGHT - deathStar.getHeight() * DEATHSTAR_IMAGE_SCALE - 2;
		deathStar.setY(deathStarPos.yPos);
	}
	if (deathStarPos.yPos - 2 <= 0) {
		deathStarVel.yVel *= -1;
		deathStarPos.yPos = 2;
		deathStar.setY(deathStarPos.yPos);
	}
}

//=============================================================================
// Render game items
//=============================================================================
void Spacewar::render()
{
	std::stringstream displayScore;
	displayScore << "\t \t \t \t \t \t \t \t \t \t"; // for aligning the score text to the screen
	displayScore << playerScore;
	displayScore << "\t \t \t SCORE \t \t \t";
	displayScore << comScore;
	graphics->spriteBegin();                // begin drawing sprites

	//sony.draw();
	background.draw();
	lightsaber.draw();
	lightsaber2.draw();
	deathStar.draw();
	textFont->setFontColor(graphicsNS::YELLOW);
	textFont->print(displayScore.str(), GAME_WIDTH/4, 10);
	if (playerScore == 11) {
		gameOverFont->setFontColor(graphicsNS::YELLOW);
		gameOverFont->print("You Win!", 100, GAME_HEIGHT/2);
		graphics->spriteEnd();
		Game::paused = true;
	}
	else if (comScore == 11) {
		gameOverFont->setFontColor(graphicsNS::YELLOW);
		gameOverFont->print("CPU Wins!", 100, GAME_HEIGHT/2);
		graphics->spriteEnd();
		Game::paused = true;
	}

	graphics->spriteEnd();                  // end drawing sprites
}

//=============================================================================
// Reset the board after a score
// bool scoredLast: true -> player, false -> computer
//=============================================================================
void Spacewar::resetGame(bool scoredLast) {
	deathStar.setX(GAME_WIDTH/2 - (deathStar.getWidth()*DEATHSTAR_IMAGE_SCALE)/2);
	deathStar.setY(GAME_HEIGHT/2 - (deathStar.getHeight()*DEATHSTAR_IMAGE_SCALE)/2);
	deathStar.setScale(DEATHSTAR_IMAGE_SCALE);

	// reset the ball speed after a score
	deathStarVel.xVel = 300;
	deathStarVel.yVel = (rand()%720) - 360;
	if(deathStarVel.yVel >= -180 && deathStarVel.yVel < 0)	// in order to guarantee we have some vertical velocity
		deathStarVel.yVel = -180;
	else if(deathStarVel.yVel <= 180 && deathStarVel.yVel >= 0) 
		deathStarVel.yVel = 180;

	if (scoredLast) { // player scored last
		deathStarVel.xVel *= 1; // ball continues toward the right/computer player
	}
	else { // computer scored last
		deathStarVel.xVel *= -1; // ball continues toward the left/human player
	}
}

//=============================================================================
// The graphics device was lost.
// Release all reserved video memory so graphics device may be reset.
//=============================================================================
void Spacewar::releaseAll()
{
	//sonyTexture.onLostDevice();
	backgroundTexture.onLostDevice();
	lightsaberTexture.onLostDevice();
	lightsaberTexture2.onLostDevice();
	deathStarTexture.onLostDevice();
	textFont->onLostDevice();
	gameOverFont->onLostDevice();

	Game::releaseAll();
	return;
}

//=============================================================================
// The grahics device has been reset.
// Recreate all surfaces.
//=============================================================================
void Spacewar::resetAll()
{

	//sonyTexture.onResetDevice();
	backgroundTexture.onResetDevice();
	lightsaberTexture.onResetDevice();
	lightsaberTexture2.onResetDevice();
	deathStarTexture.onResetDevice();
	textFont->onResetDevice();
	gameOverFont->onResetDevice();
	Game::resetAll();
	return;
}
