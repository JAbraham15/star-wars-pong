// Programming 2D Games
// Copyright (c) 2011 by: 
// Charles Kelly
// Chapter 5 spacewar.h v1.0

#ifndef _SPACEWAR_H             // Prevent multiple definitions if this 
#define _SPACEWAR_H             // file is included in more than one place
#define WIN32_LEAN_AND_MEAN

#include "game.h"
#include "textureManager.h"
#include "image.h"
#include "graphics.h"
#include "textDX.h"

//=============================================================================
// This class is the core of the game
//=============================================================================
class Spacewar : public Game
{
private:

	// My defines

	#define SONY_IMAGE_SCALE 0.4
	#define BACKGROUND_IMAGE_SCALE 1.0
	#define LIGHTSABER_IMAGE_SCALE 0.4
	#define DEATHSTAR_IMAGE_SCALE 0.4

// My vars

	bool playerSwingPlayed;
	bool comSwingPlayed;
	bool hitPlayed;
	bool collided;
	int playerScore;
	int comScore;
	int randSwing;
	int randHit;

	TextureManager sonyTexture;
	Image sony;
	TextureManager lightsaberTexture;  //red
	Image lightsaber;
	TextureManager lightsaberTexture2; //blue
	Image lightsaber2;
	TextureManager deathStarTexture;
	Image deathStar;
	TextureManager backgroundTexture;
	Image background;
	TextDX *textFont;
	TextDX *gameOverFont;

	struct position{
		float xPos;
		float yPos;
		position() {xPos = 0; yPos = 0;}
	} sonyPos, lightsaberPos, lightsaberPos2, deathStarPos;

	struct velocity{
		float xVel;
		float yVel;
		velocity() { xVel = 0; yVel = 0;}
	} sonyVel, lightsaberVel, lightsaberVel2, deathStarVel;
	
public:
    // Constructor
    Spacewar();

    // Destructor
    virtual ~Spacewar();

    // Initialize the game
    void initialize(HWND hwnd);
    void update();      // must override pure virtual from Game
    void ai();          // "
    void collisions();  // "
    void render();      // "
	void resetGame(bool scoredLast); // true -> player, false -> computer
    void releaseAll();
    void resetAll();
};

//Stuff for physics


#endif
